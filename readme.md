#Модуль оплаты платежной системы IntellectMoney для CMS Moguta

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/Moguta.CMS#MogutaFiles.

Модуль встроен в дефолтную поставку CMS.
<br>
Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/Moguta.CMS#277551077288f5f1d2f843a391795f0ad74dd7ca
