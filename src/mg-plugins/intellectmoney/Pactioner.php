<?php
//Класс для выполнения действий по запросу ajax. Данные приходят в $_POST

require_once("IntellectMoneyCommon/UserSettings.php");

class Pactioner extends Actioner {
    private $pluginName = 'intellectmoney';
  
    public function saveSettings() {
        $paysystemParams = array();
        $imUserSettings = \PaySystem\UserSettings::getInstance();
        foreach($imUserSettings->getNamesOfOrganizationParamsToSave() as $value) {
            $paysystemParams[$value] = $_POST['data'][$value];
        }
        $this->data['res'] = $paysystemParams;

        if ( DB::query("
            UPDATE " . PREFIX . "payment SET `paramArray` = '" . serialize($paysystemParams) . "' WHERE name='" . $this->pluginName ."'
        ")) {
            return true;
        }
        return false;
        
    }
}