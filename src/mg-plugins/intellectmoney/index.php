<?php
/*
  Plugin Name: IntellectMoney
  Author: IntellectMoney
  Version: 1.0
 */

require_once("IntellectMoneyCommon/LanguageHelper.php");
require_once("IntellectMoneyCommon/UserSettings.php");
require_once("IntellectMoneyCommon/Order.php");
require_once("IntellectMoneyCommon/Customer.php");
require_once("IntellectMoneyCommon/Payment.php");
require_once("IntellectMoneyCommon/VATs.php");
require_once("mg-core/models/order.php");
require_once("model.php");

new IntellectMoney;

class IntellectMoney {

    private static $lang = array();
    private static $pluginName = 'intellectmoney';
    private static $path = '';

    public function __construct() {
        mgActivateThisPlugin(__FILE__, array(__CLASS__, 'activate')); //Действие при активации плагина
        mgAddAction(__FILE__, array(__CLASS__, 'pageSettingsPlugin'));  //Действие при открытии админки
        mgAddAction('models_order_addorder' , array(__CLASS__, 'getForm'), 1); //Действие при оплате (вызове метода могуты models_order_addorder, можно повесить действие на любой метод)

        self::$lang = PM::plugLocales(self::$pluginName);
        self::$path = PLUGIN_DIR . self::$pluginName;
    }

    public static function activate() {
        USER::AccessOnly('1,4', 'exit()');
        IntellectMoneyModel::insertPluginSettingsIntoDataBase();
    }

    public static function pageSettingsPlugin() {
        USER::AccessOnly('1,4', 'exit()');

        $paramsForInputTypeText = array(
            'eshopId',
            'secretKey',
            'expireDate',
            'holdTime',
            'successUrl',
            'preference',
            'group',
        );
        $paramsForInputTypeCheckBox = array(
            'testMode',
            'holdMode',
        );
        $paramsForSelect = array(
            'tax',
            'deliveryTax',
        );
        $statuses = array(
            'statusCreated',
            'statusCancelled',
            'statusPaid',
            'statusHolded',
            'statusPartiallyPaid',
            'statusRefunded',
        );

        $orderModel = new Models_Order;
        $CMSStatusList = array();
        for ($i = 0; $i <= 6; $i++) {
            $status['status_id'] = $i;
            $CMSStatusList[$i] = $orderModel->getOrderStatus($status);
        }

        $pluginName = self::$pluginName;
        $lang = array_merge(self::$lang, self::getLangArray());
        $VATs = \PaySystem\VATs::getList();
        $paySystemParams = IntellectMoneyModel::getPaySystemParams();
        require_once('pageplugin.php');
    }

    private static function getLangArray($lang = 'ru') {
        $LanguageHelper = \PaySystem\LanguageHelper::getInstance($lang);
        $UserSettings = \PaySystem\UserSettings::getInstance();

        $text = array();
        foreach ($UserSettings->getNamesOfOrganizationParamsToSave() as $paramName) {
            $text['title'][$paramName] = $LanguageHelper->getTitle($paramName);
            $text['desc'][$paramName] = $LanguageHelper->getDesc($paramName);
        }
        return $text;
    }

    public static function getForm($addOrderData) {
        $order = IntellectMoneyModel::getOrder($addOrderData['result']['id']);
        $orderItems = unserialize(stripslashes($order['order_content']));
        $recipientAmount = IntellectMoneyModel::getRecipientAmount($order['summ'], $order['delivery_cost']);

        $settings = IntellectMoneyModel::getPaySystemParams();
        $settings['testMode'] = $settings['testMode'] == 'true' ? 1 : 0;
        $settings['holdMode'] = $settings['holdMode'] == 'true' ? 1 : 0;
        $imUserSettings = \PaySystem\UserSettings::getInstance($settings);
        $imCustomer = PaySystem\Customer::getInstance($_POST['email'], $_POST['fio'], $_POST['phone']);
        $imOrder = \PaySystem\Order::getInstance($order['InvoiceId'], $addOrderData['result']['orderNumber'], $recipientAmount, $recipientAmount, 0, $order['delivery_cost'], $order['currency_iso'], 0, NULL);
        foreach ($orderItems as $item) {
            $imOrder->addItem($item['fulPrice'], $item['count'], $item['name'], $imUserSettings->getTax());
        }
        if(isset($order['delivery_cost'])) {
            $imOrder->addItem($order['delivery_cost'], 1, 'Delivery', $imUserSettings->getDeliveryTax());
        }
        
        $imPayment = \PaySystem\Payment::getInstance($imUserSettings, $imOrder, $imCustomer);
        $imUserSettings->setMerchantUrl("https://merchant.intellectmoney.ru");
        
        echo $imPayment->generateForm(true, true);
    }

}
