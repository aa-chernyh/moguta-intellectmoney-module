<?php

require_once("IntellectMoneyCommon/UserSettings.php");
require_once("IntellectMoneyCommon/Order.php");
require_once("IntellectMoneyCommon/Result.php");
require_once("IntellectMoneyCommon/Status.php");
require_once("IntellectMoneyCommon/IntellectMoneyBase.php");

new IntellectMoneyResult;

class IntellectMoneyResult {

    public function __construct() {
        $this->start();
    }

    public function start() {
        PaySystem\LanguageHelper::getInstance('en');
        $imUserSettings = PaySystem\UserSettings::getInstance(IntellectMoneyModel::getPaySystemParams());

        $orderId = IntellectMoneyModel::getOrderIdByOrderNumber($_REQUEST['orderId']);
        if(empty($orderId)){
            if($_REQUEST['paymentStatus'] == \PaySystem\Status::paid) {
                echo "Поступила оплата по удалённому заказу номер " . $_REQUEST['orderId'] . ". Номер счета в системе IntellectMoney - " . $_REQUEST['paymentId'];
            } else {
                echo 'OK';
            }
            die;
        }
        $order = IntellectMoneyModel::getOrder($orderId);
        $recipientAmount = IntellectMoneyModel::getRecipientAmount($order['summ'], $order['delivery_cost']);
        $imOrder = \PaySystem\Order::getInstance($_REQUEST['paymentId'], $_REQUEST['orderId'], $recipientAmount, $recipientAmount, 0, $order['delivery_cost'], $order['currency_iso'] == "RUR" ? "RUB" : "TST", 0, IntellectMoneyModel::getStatusIm($orderId));
        
        $imResult = \PaySystem\Result::getInstance($_REQUEST, $imUserSettings, $imOrder);
        $response = $imResult->processingResponse();
        
        if ($response->changeStatusResult) {
            IntellectMoneyModel::setInvoiceIdAndPaymentStatus($orderId, $imOrder->getInvoiceId(), $_REQUEST['paymentStatus']);
            IntellectMoneyModel::updateOrderStatus($orderId, $response->statusCMS);
        }

        echo $imResult->getMessage();
        die;
    }

}
