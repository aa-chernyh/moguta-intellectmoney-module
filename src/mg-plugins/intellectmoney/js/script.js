 /* 
 * Модуль  IntellectMoneyModule, подключается на странице настроек плагина.
 */

$(document).ready(function(){
    $('#saveSettings').click(function(){
        saveSettings();
    });
});

function saveSettings(){
    $('.section-intellectmoney .text-success, .section-intellectmoney .text-warning').addClass('hide');
    
    let paramNames = [
        'eshopId', 'secretKey', 'expireDate', 'holdTime',
        'successUrl', 'preference', 'group', 'testMode',
        'holdMode', 'tax', 'deliveryTax', 'statusCreated',
        'statusCancelled', 'statusPaid', 'statusHolded',
        'statusPartiallyPaid', 'statusRefunded'
    ];
    var paramsToSave = {};
    paramNames.forEach(function(paramName){
        paramsToSave[paramName] = $('.section-intellectmoney [name=' + paramName + ']').val();
    });

    admin.ajaxRequest({
        mguniqueurl: "action/saveSettings",
        pluginHandler: 'intellectmoney',
        data: paramsToSave
    },
    function(response) {
        if(response.status == "success"){
            $('.section-intellectmoney .text-success').removeClass('hide');
        } else {
            $('.section-intellectmoney .text-warning').removeClass('hide');
        }
    });
}