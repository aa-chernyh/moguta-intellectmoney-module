<!--
Страница настроек

Доступны переменные:
  $pluginName - название плагина
  $lang - массив фраз для выбранной локали движка
  $paySystemParams - набор параметров плагина
-->

<link rel="stylesheet" href="<?= SITE ?>/<?= self::$path ?>/css/style.css" type="text/css" />     
<script type="text/javascript">
    includeJS("<?= SITE ?>/<?= self::$path ?>/js/script.js");  
</script>

<div class="section-<?php echo $pluginName ?>">
    <div class="widget-table-body">
        <div class="wrapper-entity-setting">
            <div class="widget-table-action base-settings">
                <ul class="list-option">
                    <li class="section"><?php echo $lang['SECTION_SERVICE_SETTINGS']; ?></li>
                    <?php foreach($paramsForInputTypeText as $paramCode) { ?>
                        <li>
                            <label>
                                <span class="custom-text"><?= $lang['title'][$paramCode] ?></span>
                                <input type="text" name="<?=$paramCode?>" value="<?= $paySystemParams[$paramCode] ?>" class="tool-tip-right" title="<?php echo $lang['desc'][$paramCode] ?>"/>
                            </label>
                        </li>
                        <input type="hidden" name="payment_id" value="<?php echo $paySystemParams['payment_id'] ?>"/>
                    <?php } foreach($paramsForInputTypeCheckBox as $paramCode) { ?>
                        <li>
                            <label>
                                <span class="custom-text"><?= $lang['title'][$paramCode] ?></span>
                                <input type="checkbox" name="<?=$paramCode?>" value="<?= $paySystemParams[$paramCode] ?>" class="tool-tip-right" title="<?php echo $lang['desc'][$paramCode] ?>" <?php if($paySystemParams[$paramCode] == "true") { echo "checked"; }?>/>
                            </label>
                        </li>
                        <input type="hidden" name="payment_id" value="<?php echo $paySystemParams['payment_id'] ?>"/>
                    <?php } foreach($paramsForSelect as $paramCode) { ?>
                        <li>
                            <label>
                                <span class="custom-text"><?= $lang['title'][$paramCode] ?></span>
                                <select name="<?= $paramCode ?>">
                                    <?php foreach($VATs as $vat) { ?>
                                        <option value="<?= $vat['id'] ?>" <?php if($vat['id'] == $paySystemParams[$paramCode]) { echo "selected"; } ?>><?= $vat['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </label>
                        </li>
                    <?php } foreach($statuses as $paramCode) { ?>
                        <li>
                            <label>
                                <span class="custom-text"><?= $lang['title'][$paramCode] ?></span>
                                <select name="<?= $paramCode ?>">
                                    <?php foreach($CMSStatusList as $statusCode => $statusName) { ?>
                                        <option value="<?= $statusCode ?>" <?php if($statusCode == $paySystemParams[$paramCode]) { echo "selected"; } ?>><?= $statusName ?></option>
                                    <?php } ?>
                                </select>
                            </label>
                        </li>
                    <?php } ?>
                    <li class="section hide text-success"><?=$lang['ENTITY_SAVE']?></li>
                    <li class="section hide text-warning"><?=$lang['ENTITY_DEL_NOT']?></li>
                    <li>
                        <input type="hidden" name="action" value="saveParams">
                        <button class="save-button trigger tool-tip-bottom button success" title="<?= $lang['buttonSave'] ?>" id="saveSettings">
                            <span>Сохранить</span>
                        </button>
                    </li>
                </ul>              
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
