<?php
require_once("IntellectMoneyCommon/UserSettings.php");

class IntellectMoneyModel {
    private static $pluginName = 'intellectmoney';
    
    public static function getOrder($id) {
        $res = DB::query("
            SELECT o.*, im.invoiceId, im.statusIm FROM `" . PREFIX . "order` o 
            LEFT JOIN " . PREFIX . self::$pluginName . "_order_data im ON o.id = im.OrderId 
            WHERE o.Id = " . $id . " LIMIT 1");
        return DB::fetchAssoc($res);
    }
    
    public static function getPaySystemParams() {
        $res = DB::query("
            SELECT `paramArray` FROM " . PREFIX . "payment WHERE name='" . self::$pluginName . "'
        ");
        $row = DB::fetchAssoc($res);
        return unserialize($row['paramArray']);
    }
    
    public static function insertPluginSettingsIntoDataBase() {
        $res = DB::query("SELECT id FROM " . PREFIX . "payment WHERE name='" . self::$pluginName . "'");
        if (!DB::numRows($res)) {
            $imUserSettings = \PaySystem\UserSettings::getInstance();
            foreach ($imUserSettings->getNamesOfOrganizationParamsToSave() as $value) {
                $defaults[$value] = NULL;
            }
            
            DB::query("
                INSERT INTO " . PREFIX . "payment (`id`, `name`, `activity`, `paramArray`, `urlArray`, `rate`, `sort`, `add_security`, `permission`) 
                VALUES (NULL, '" . self::$pluginName . "', 1, '" . serialize($defaults) . "', NULL, 0, 0, 0, 'fiz')
            ");
            
            DB::query("
                CREATE TABLE IF NOT EXISTS `" . PREFIX . self::$pluginName . "_order_data` (
                    `OrderId` int(11),
                    `InvoiceId` bigint(10) DEFAULT NULL,
                    `StatusIm` int(1) DEFAULT NULL,
                    PRIMARY KEY (OrderId)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ");
        }
    }
    
    public static function getOrderIdByOrderNumber($orderNumber) {
        $res = DB::query("
            SELECT id FROM " . PREFIX . "order WHERE number = '" . $orderNumber . "'
        ");
        $res = DB::fetchAssoc($res);
        return $res['id'];
    }
    
    public static function updateOrderStatus($orderId, $statusCMS) {
        return DB::query("
            UPDATE " . PREFIX . "order SET status_id = '" . $statusCMS . "' WHERE id = '" . $orderId . "'
        ");
    }
    
    public static function setInvoiceIdAndPaymentStatus($orderId, $invoiceId, $statusIm) {
        if(empty(self::getStatusIm($orderId))){
            return DB::query("
                INSERT INTO " . PREFIX . self::$pluginName . "_order_data (`OrderId`, `InvoiceId`, `StatusIm`) 
                VALUES ('" . $orderId . "', " . $invoiceId . ", " . $statusIm . ")
            ");
        } else {
            self::setStatusIm($orderId, $statusIm);
        }
        return true;
    }
    
    private static function setStatusIm($orderId, $statusIm) {
        return DB::query("
            UPDATE " . PREFIX . self::$pluginName . "_order_data SET StatusIm = '" . $statusIm . "' WHERE OrderId = '" . $orderId . "'
        ");
    }
    
    public static function getStatusIm($orderId) {
        $res = DB::query("
            SELECT StatusIm FROM " . PREFIX . self::$pluginName . "_order_data WHERE OrderId = '" . $orderId . "'
        ");
        $res = DB::fetchAssoc($res);
        return $res['StatusIm'];
    }
    
    public static function getRecipientAmount($orderPrice, $deliveryPrice) {
        return empty($deliveryPrice) ? $orderPrice : $orderPrice + $deliveryPrice;
    }
}

